﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Sorting
{

 public class HeapSort<T> : ISort<T> where T : struct, IComparable
 {
  int _count;
  T[] _arr;
  public HeapSort(T[] arr)
  {
   _arr = arr;
  }

  protected int Len { get { return _arr.Length; } }
  public T[] MaxHeapify()
  {
   for (int i = Len / 2; i >= 0; i--)
   {
    MaxHeapifyRec(_arr, i, Len);
   }
   return _arr;
  }

  public T[] Sort(List<string> sortLog)
  {
   _count = 0;
   MaxHeapify();
   Console.WriteLine(String.Format("Heap arr:{0}, count:{1}", string.Join(",", _arr), _count));
   int recLen = Len - 1;
   while (recLen > 0)
   {
    T temp = _arr[0];
    _arr[0] = _arr[recLen];
    _arr[recLen] = temp;
    recLen = recLen - 1;
    MaxHeapifyRec(_arr, 0, recLen);
    Console.WriteLine(String.Format("Heap arr:{0}, recLen:{1}, count:{2}", string.Join(",", _arr), recLen, _count));
   }
   return _arr;
  }

  protected void MaxHeapifyRec(T[] arr, int i, int len)
  {
   _count++;
   int l = i * 2;
   int r = l + 1;
   int largest = i;
   if (l < len && arr[largest].CompareTo(arr[l]) < 0) largest = l;
   if (r < len && arr[largest].CompareTo(arr[r]) < 0) largest = r;
   if (largest != i)
   {
    T temp = arr[i];
    arr[i] = arr[largest];
    arr[largest] = temp;
    MaxHeapifyRec(arr, largest, len);
   }
  }



  // objective: make bin tree such that value at each node is greater or equal to all it's children
  //   //0, 1,2,3,4,5,6,7,8,9,10
  // len =  11, mid = 6, pick i=6
  // max(a[6], a[2*6], a[2*6+1)]) = a[11]
  // a[6] = 6
  // i = 5
  //  max(a[5], a[10], a[11)]) = a[10]
  // a[5] = 10, a[10] = 5  [0,1,2,3,4,10,6,7,8,9,5]
  //  i = 10
  // i=4
  // max(a[4], a[8], a[9)]) = a[9]
  // a[4] =  9, a[9] = 4 [0,1,2,3,9,10,6,7,8,4,5]
  // i = 9
  // i = 3
  // max(a[3], a[6], a[7)]) = a[7]
  // a[7] =  3, a[3] = 7 [0,1,2,7,9,10,6,3,8,4,5]
  // i = 7
  // i =2
  // etc..
 }
 }
