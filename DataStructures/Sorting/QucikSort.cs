﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructures.Sorting
{

 public class QuickSort<T> : ISort<T> where T : struct, IComparable
 {
  T[] _arr;
  int recursionCnt = 0;
  public QuickSort(T[] arr)
  {
   _arr = arr;
  }
  public T[] Sort(List<string> sortLog)
  {
   DoQuickSort(_arr, 0, _arr.Length - 1, sortLog);
   Console.WriteLine(String.Format("result:{0}", string.Join(",", _arr)));
   return _arr;
  }

  protected void DoQuickSort(T[] arr, int low, int high, List<string> sortLog)
  {
   recursionCnt++;
   Console.WriteLine(String.Format("{0}low:{1} high:{2}, arr:[{3}],", new String('-', recursionCnt), low, high, string.Join(",", arr.Skip(low).Take(high - low + 1).ToArray())));
   if (low < high)
   {
    int pivot = Partition(arr, low, high, sortLog);
    Console.WriteLine(String.Format("pivot:{0}, arr[pivot]:{1}", pivot, arr[pivot]));
    if (pivot < high)
    {
     if (pivot > low)
      DoQuickSort(arr, low, pivot, sortLog);
     if (pivot < high - 1)
      DoQuickSort(arr, pivot + 1, high, sortLog);
    }
   }
   recursionCnt--;
  }

  protected int Partition(T[] arr, int low, int high, List<string> sortLog)
  {
   T pivot = arr[high];
   int left = low;
   int right = high;
   Console.WriteLine(String.Format("pivot:{0}, low:{1}, high:{2}", pivot, low, high));
   while (left < right)
   {
    while (left < high && arr[left].CompareTo(pivot) < 0)
    {
     left++;
    }

    while (right > low && arr[right].CompareTo(pivot) > 0)
    {
     right--;
    }
    Console.WriteLine(String.Format("left:{0}, right:{1}", left, right));
    if (left <= right)
    {
     Swap(arr, left, right, sortLog);
     left++; right--;
    }
   }
   return right;

  }

  protected void Swap(T[] arr, int i, int j, List<string> sortLog)
  {
   string log = String.Format("Swap {0} with {1}", arr[i], arr[j]);
   T temp = arr[i];
   arr[i] = arr[j];
   arr[j] = temp;
   Console.WriteLine(String.Format("{0}{1} result:[{2}]", new String('-', recursionCnt), log, string.Join(",", arr)));
  }

 }
}
