﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Sorting
{
 public class MergeSort<T> : ISort<T> where T : struct, IComparable
 {
  T[] _arr;
  int recursionCnt = 0;
  public MergeSort(T[] arr)
  {
   _arr = arr;
  }
  public T[] Sort(List<string> sortLog)
  {
   return Sort(_arr, sortLog);

  }

  protected T[] Sort(T[] arr, List<string> sortLog)
  {
   if (arr.Length <= 1)
    return arr;
   recursionCnt++;
   int mid = (arr.Length - 1) / 2;
   sortLog.Add(string.Format("{0}mid:{1} arr[]:[{2}]", new String('-', recursionCnt), mid, string.Join(",", arr)));
   T[] left = new T[mid + 1];
   T[] right = new T[arr.Length - mid - 1];
   for (int i = 0; i < left.Length; i++) left[i] = arr[i];
   for (int i = 0; i < right.Length; i++) right[i] = arr[i + mid + 1];
   sortLog.Add(string.Format("{0}left[]:[{1}]", new String('-', recursionCnt), string.Join(",", left)));
   left = Sort(left, sortLog);
   sortLog.Add(string.Format("{0}right[]:[{1}]", new String('-', recursionCnt), string.Join(",", right)));
   right = Sort(right, sortLog);
   recursionCnt--;
   return MergeArrays(left, right, sortLog);

  }

  protected T[] MergeArrays(T[] left, T[] right, List<string> sortLog)
  {

   int resultLen = left.Length + right.Length;
   T[] result = new T[resultLen];
   int leftIndex = 0, rightIndex = 0;
   while (leftIndex < left.Length && rightIndex < right.Length)
   {
    if (left[leftIndex].CompareTo(right[rightIndex]) <= 0)
    {
     result[leftIndex + rightIndex] = left[leftIndex];
     leftIndex++;
    }
    else
    {
     result[leftIndex + rightIndex] = right[rightIndex];
     rightIndex++;
    }

   }

   while (leftIndex < left.Length)
   {
    result[leftIndex + rightIndex] = left[leftIndex];
    leftIndex++;
   }

   while (rightIndex < right.Length)
   {
    result[leftIndex + rightIndex] = right[rightIndex];
    rightIndex++;
   }
   sortLog.Add(string.Format("{0}Merging left:[{1}], right:[{2}], result:[{3}]", new String('-', recursionCnt), string.Join(",", left), string.Join(",", right), string.Join(",", result)));
   return result;

  }

  protected void Swap(T[] arr, int lowIndex, int highIndex)
  {
   if (arr[lowIndex].CompareTo(arr[highIndex]) > 0)
   {
    T temp = arr[lowIndex];
    arr[lowIndex] = arr[highIndex];
    arr[highIndex] = temp;
   }
  }
 }
}
