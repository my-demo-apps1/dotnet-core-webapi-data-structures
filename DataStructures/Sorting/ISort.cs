﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Sorting
{
 public interface ISort<T> where T : struct, IComparable
 {
  T[] Sort(List<string> searchTrace);
 }
}
