﻿using DataStructures.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures
{
 public class BalanceArray
 {
  public static int MinValueToBalance(int[] a, ref int side /* -1=left, 0=balnced, 1= right*/ )
  {
   int minValuetoBalance;
   int arrayLen = a.Length;
   int sumLeft = 0;
   for (int i = 0; i < arrayLen / 2; i++)
   {
    sumLeft += a[i];
   }
   int sumRight = 0;
   for (int i = arrayLen / 2; i < arrayLen; i++)
   {
    sumRight += a[i];
   }
   minValuetoBalance = (sumLeft - sumRight);
   if (minValuetoBalance < 0)
   {
    side = -1;
   }
   else if (minValuetoBalance > 0)
   {
    side = 1;
   }

   return Math.Abs(minValuetoBalance);
  }

  public static int[] Balance(int[] a)
  {
   int side = 0; /* -1=left, 0=balnced, 1= right*/

   int minValuetoBalance = MinValueToBalance(a, ref side);

   if (side == -1)
   {
    a = a.Prepend(minValuetoBalance); ;
   }
   else if (side == 1)
   {
    a = a.Append(minValuetoBalance); ;
   }

   return a;

  }
 }
}
