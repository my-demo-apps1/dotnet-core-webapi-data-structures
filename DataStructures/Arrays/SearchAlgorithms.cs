﻿using System;
using System.Collections.Generic;

namespace DataStructures
{
 public class SearchAlgorithms
 { 
  /// <summary>
  /// Returns index of match
  /// </summary>
  /// <param name="arr"></param>
  /// <param name="key"></param>
  /// <returns></returns>
  public static int BinarySearch(int[] arr, int key, List<string> searchTrace )
  {
   int minNum = 0;
   int maxNum = arr.Length - 1;

   while (minNum <= maxNum)
   {
    int mid = (minNum + maxNum) / 2;
    searchTrace.Add(String.Format("min:{0}, mid:{1}, max:{2}, arr[{3}]:{4}", minNum, mid, maxNum, mid, arr[mid]));
    if (key == arr[mid])
    {
     return mid;
    }
    else if (key < arr[mid])
    {
     maxNum = mid - 1;
    }
    else
    {
     minNum = mid + 1;
    }
   }
   return -1;
  }


 }
}
