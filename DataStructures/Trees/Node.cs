﻿using DataStructures.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Trees
{
 public class Node<T> where T : struct, IComparable
 {
  public Node()
  {

  }

  public Node(T data)
  {
   this.Data = data;
  }
  public T Data;
  public Node<T> Left;
  public Node<T> Right;
 }

}
