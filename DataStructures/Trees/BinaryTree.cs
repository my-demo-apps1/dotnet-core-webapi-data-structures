﻿using DataStructures.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Trees
{
 public class BinarySearchTree<T> where T : struct, IComparable
 {

  public Node<T> Root;

  public BinarySearchTree()
  {
   Root = null;
  }

  public void Insert(T i)
  {
   Node<T> newNode = new Node<T>(i);
   if (this.Root == null)
   {
    this.Root = newNode;
   }
   else
   {
    InsertNode(Root, newNode);
   }
  }

  public void InsertNode(Node<T> current, Node<T> newNode)
  {

   if (current == null)
   {
    current = newNode;
   }
   else
   {
    bool isInserted = false;
    if (current.Left == null && newNode.Data.CompareTo(current.Data) < 0)
    {
     current.Left = newNode;
     isInserted = true;
    }

    if (current.Right == null && newNode.Data.CompareTo(current.Data) >= 0)
    {
     current.Right = newNode;
     isInserted = true;
    }
    if (!isInserted)
    {
     if (current.Left != null && newNode.Data.CompareTo(current.Data) < 0)
     {
      InsertNode(current.Left, newNode);
     }
     if (current.Right != null && newNode.Data.CompareTo(current.Data) >= 0)
     {
      InsertNode(current.Right, newNode);
     }
    }
   }

  }

  public Node<T> GetBalancedTree()
  {
   List<T> inOrder = TraverseNode<T>.GetInOrder(Root);
   return BalanceTree(inOrder.ToArray(), 0, inOrder.Count);
  }

  private Node<T> BalanceTree(T[] inOrder, int minIndex, int maxIndex)
  {
   if (minIndex > maxIndex)
   {
    return null;
   }
   int mid = minIndex + (maxIndex - minIndex) / 2;

   if (inOrder.Length > mid)
   {
    T newNodeData = inOrder[mid];
    Node<T> newNode = new Node<T>(newNodeData);
    Node<T> left = BalanceTree(inOrder, minIndex, mid - 1);
    if (left != null)
    {
     newNode.Left = left;
    }
    Node<T> right = BalanceTree(inOrder, mid + 1, maxIndex);
    if (right != null)
    {
     newNode.Right = right;
    }

    return newNode;
   }
   return null;
  }

 }

}
