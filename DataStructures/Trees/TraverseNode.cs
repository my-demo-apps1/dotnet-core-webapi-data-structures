﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Trees
{
 public class TraverseNode<T> where T: struct, IComparable
 {

  /// <summary>
  /// In Order Traversal: Left, Root, Right
  /// </summary>
  /// <param name="current"></param>
  /// <returns></returns>
  public static List<T> GetInOrder(Node<T> current)
  {
   List<T> nodesInOrder = new List<T>();
   if (current != null)
   {
    if (current.Left != null)
    {
     nodesInOrder.AddRange(GetInOrder(current.Left));
    }

    nodesInOrder.Add(current.Data);

    if (current.Right != null)
    {
     nodesInOrder.AddRange(GetInOrder(current.Right));
    }
   }
   return nodesInOrder;
  }
  /// <summary>
  /// Pre Order Traversal: Root, Left, Right, 
  /// </summary>
  /// <param name="current"></param>
  /// <returns></returns>
  public static List<T> GetPreOrder(Node<T> current)
  {
   List<T> nodesInOrder = new List<T>();
   nodesInOrder.Add(current.Data);

   if (current != null)
   {
    if (current.Left != null)
    {
     nodesInOrder.AddRange(GetPreOrder(current.Left));
    }


    if (current.Right != null)
    {
     nodesInOrder.AddRange(GetPreOrder(current.Right));
    }
   }
   return nodesInOrder;
  }


  /// <summary>
  /// Post Order Traversal: Left, Right, Root
  /// </summary>
  /// <param name="current"></param>
  /// <returns></returns>
  public static List<T> GetPostOrder(Node<T> current)
  {
   List<T> nodesInOrder = new List<T>();
   if (current != null)
   {
    if (current.Left != null)
    {
     nodesInOrder.AddRange(GetPostOrder(current.Left));
    }


    if (current.Right != null)
    {
     nodesInOrder.AddRange(GetPostOrder(current.Right));
    }
   }

   nodesInOrder.Add(current.Data);
   return nodesInOrder;
  }

 }
}
