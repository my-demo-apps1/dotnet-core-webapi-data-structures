﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Extensions
{
 public static class Extensions
 {
  public static T[] Append<T>(this T[] array, T item)
  {
   if (array == null)
   {
    return new T[] { item };
   }

   T[] result = new T[array.Length + 1];
   for (int i = 0; i < array.Length; i++)
   {
    result[i] = array[i];
   }

   result[array.Length] = item;
   return result;
  }

  public static T[] Prepend<T>(this T[] array, T item)
  {
   if (array == null)
   {
    return new T[] { item };
   }

   T[] result = new T[array.Length + 1];
   result[0] = item;
   for (int i = 0; i < array.Length; i++)
   {
    result[i + 1] = array[i];
   }
   return result;
  }
 }
}
