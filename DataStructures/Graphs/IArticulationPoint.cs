﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Graphs
{
 public interface IArticulationPoint
 {
  List<int> GetAP(int v, List<int>[] adjacentNodesList);
 }
}
