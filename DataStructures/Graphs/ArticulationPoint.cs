﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Graphs
{
 public  class ArticulationPoint: IArticulationPoint
 {
  int time = 0;

  public List<int> GetAP( int v, List<int>[] adjacentNodesList)
  {
   List<int> result = new List<int>();
   bool[] ap = new bool[v];
   bool[] visited = new bool[v];
   int[] parent = new int[v];
   int[] firstDiscTime = new int[v];
   int[] lastDiscTime = new int[v];
   for (int i = 0; i < v; i++)
   {
    parent[i] = -1;
   }

   for (int i = 0; i < v; i++)
   {
    Console.WriteLine(String.Format("---->curNode:{0}", i));
    if (!visited[i])
    {
      GetAP(i, visited, parent, ap, firstDiscTime, lastDiscTime, adjacentNodesList);
    }
    if (ap[i]) result.Add(i);
   }
   return result;
  }

  protected bool[] GetAP(int curNode, bool[] visited, int[] parent, bool[] ap, int[] firstDiscTime, int[] lastDiscTime, List<int>[] adjacentNodesList)
  {
   visited[curNode] = true;
   firstDiscTime[curNode] = lastDiscTime[curNode] = time++;
   int children = 0;
   foreach (int adjNode in adjacentNodesList[curNode])
   {
    if (!visited[adjNode])
    {
     parent[adjNode] = curNode;
     children++;
     GetAP(adjNode, visited, parent, ap, firstDiscTime, lastDiscTime, adjacentNodesList);

     firstDiscTime[curNode] = Math.Min(firstDiscTime[curNode], firstDiscTime[adjNode]);

     if (parent[curNode] == -1 && children > 1)
      ap[curNode] = true;

     if (parent[curNode] != -1 && firstDiscTime[adjNode] >= lastDiscTime[curNode])
     {
      ap[curNode] = true;
     }

    }
    else if (parent[curNode] != adjNode)
    {
     firstDiscTime[curNode] = Math.Min(lastDiscTime[adjNode], firstDiscTime[curNode]);
    }
    Console.WriteLine(String.Format("curNode:{0}, adjNode:{1}, children:{2}, lastDiscTime[{3}]:{4}, firstDiscTime[{5}]:{6} ", curNode, adjNode, children, curNode, lastDiscTime[curNode], adjNode, firstDiscTime[adjNode]));
   }
   return ap;
  }
 }
}
