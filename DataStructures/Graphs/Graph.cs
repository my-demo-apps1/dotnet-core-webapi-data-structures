﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataStructures.Graphs
{

 public class Graph
 {

  /*
   * Input: numNodes = 7, numEdges = 7, edges = [[0, 1], [0, 2], [1, 3], [2, 3], [2, 5], [5, 6], [3, 4]]
   */
  protected int _v;
  protected List<int>[] _adjacentNodesList;

  IArticulationPoint _articulationPoint;

  public Graph(int v, IArticulationPoint articulationPoint)
  {
   _v = v;
   _articulationPoint = articulationPoint;
   // init lenfth adj lists
   _adjacentNodesList = new List<int>[v];
   // init adj lists
   for (int i = 0; i < _adjacentNodesList.Length; i++)
   {
    _adjacentNodesList[i] = new List<int>();
   }

  }

  public void AddEdge(int v, int w)
  {
   _adjacentNodesList[v].Add(w);
  }

  protected void DFSRec(int vCur, List<int> result, bool[] visited)
  {
   visited[vCur] = true;
   result.Add(vCur);
   List<int> curAdjNodes = _adjacentNodesList[vCur];
   foreach (int ajNode in curAdjNodes)
   {
    if (!visited[ajNode])
    {
     DFSRec(ajNode, result, visited);
    }
   }
  }
  public List<int> DFS(int vCur)
  {
   List<int> result = new List<int>();

   bool[] visited = new bool[_v];
   DFSRec(vCur, result, visited);
   return result;
  }

  void BFSRec(Queue<int> curNodesToVisit, bool[] visited, List<int> result)
  {

   Queue<int> nextNodesToVisit = new Queue<int>();
   while (curNodesToVisit.Count > 0)
   {

    int curNode = curNodesToVisit.Dequeue();
    if (!visited[curNode])
    {
     result.Add(curNode);
     visited[curNode] = true;
     foreach (int adjNode in _adjacentNodesList[curNode])
     {
      if (!visited[adjNode])
       nextNodesToVisit.Enqueue(adjNode);
     }
    }
   }
   if (nextNodesToVisit.Count > 0)
    BFSRec(nextNodesToVisit, visited, result);
  }
  public List<int> BFS(int curNode)
  {
   // int[,] edges = new int[,] { { 0, 1 }, { 0, 2 }, { 1, 3 }, { 2, 3 }, { 2, 5 }, { 5, 6 }, { 3, 4 } };
   // BFS(0) = 0, 1, 2, 3, 5, 4, 6

   List<int> result = new List<int>();
   bool[] visited = new bool[_v];
   Queue<int> nextNodesToVisit = new Queue<int>();
   nextNodesToVisit.Enqueue(curNode);
   BFSRec(nextNodesToVisit, visited, result);
   return result;
  }

  int time = 0;
  public List<int> ArticulationPoints()
  {
   // Input: n = 5, edges = [[1, 2], [1, 3], [3, 4], [1, 4], [4, 5]]
   return _articulationPoint.GetAP(_v, _adjacentNodesList);

  }

 }
}
