using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace DataStructures.WebAPI
{
 public class Startup
 {
  public const string AppS3BucketKey = "AppS3Bucket";

  public Startup(IConfiguration configuration)
  {
   Configuration = configuration;
  }

  public static IConfiguration Configuration { get; private set; }

  // This method gets called by the runtime. Use this method to add services to the container
  public void ConfigureServices(IServiceCollection services)
  {
   services.AddCors(o => o.AddPolicy("ALLOW_ALL", builder =>
   {
    builder.AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader();
   }));
   services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
   services.Configure<MvcOptions>(options =>
   {
    options.Filters.Add(new CorsAuthorizationFilterFactory("ALLOW_ALL"));
   });

   services.AddSwaggerGen(c =>
   {
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Datastructure API", Version = "v1" });
   });
   // Add S3 to the ASP.NET Core dependency injection framework.
   services.AddAWSService<Amazon.S3.IAmazonS3>();
  }

  // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
  public void Configure(IApplicationBuilder app, IHostingEnvironment env)
  {
   if (env.IsDevelopment())
   {
    app.UseDeveloperExceptionPage();
   }
   else
   {
    app.UseHsts();
   }
   // Enable Cors
   app.UseCors("ALLOW_ALL");
   app.UseStaticFiles(new StaticFileOptions()
   {
    FileProvider = new PhysicalFileProvider(
                           Path.Combine(Directory.GetCurrentDirectory(), @"Static")),
    RequestPath = new PathString("/swaggerui")
   });
   app.UseSwagger();
   app.UseHttpsRedirection();
   app.UseMvc();
  }
 }
}
