﻿using DataStructures.Trees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class BinaryTreeResult<T>: ArrayParam<T> where T: struct, IComparable
 {
  public BinaryTreeResult() : base()
  { }

  public BinaryTreeResult(T[] numbersArray, Node<T> root, List<T> inOrder, List<T> preOrder, List<T> postOrder) : base(numbersArray)
  {
   this.Root = root;
   this.InOrder = inOrder;
   this.PreOrder = preOrder;
   this.PostOrder = postOrder;
  }

  public Node<T> Root { get; set; }
  public List<T> InOrder { get; set; }
  public List<T> PreOrder { get; set; }
  public List<T> PostOrder { get; set; }

 }
}
