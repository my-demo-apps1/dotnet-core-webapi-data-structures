﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class ArraySearchParams: ArrayParam<int>
 {
  public ArraySearchParams(): base()
  { }
  public ArraySearchParams(int[] numbersArray, int numberToSearch):base(numbersArray)
  {
   this.NumberToSearch = numberToSearch;
  }
  public int NumberToSearch { get; set; }
 }
}
