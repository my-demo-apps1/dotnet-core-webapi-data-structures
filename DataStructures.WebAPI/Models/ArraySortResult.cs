﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class ArraySortResult: ArraySortParams
 {
  public ArraySortResult() : base()
  { }

  public ArraySortResult(int[] numbersArray, int[] sortedArray, string sortAlgorithm, List<string> sortLog) : base(numbersArray, sortAlgorithm)
  {
   this.SortedArray = sortedArray;
   this.SortLog = sortLog;
  }

  public int[] SortedArray { get; set; }
  public List<string> SortLog { get; set; }

 }
}
