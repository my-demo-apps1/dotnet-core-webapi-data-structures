﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class ArrayParam<T> where T: struct, IComparable
 {

  public ArrayParam()
  { }

  public ArrayParam(T[] numbersArray)
  {
   this.NumbersArray = numbersArray;
  }
  public T[] NumbersArray { get; set; }
 }
}
