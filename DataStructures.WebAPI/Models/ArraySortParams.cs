﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class ArraySortParams : ArrayParam<int>
 {
  public ArraySortParams() : base()
  { }
  public ArraySortParams(int[] numbersArray, string sortAlgotirhm) : base(numbersArray)
  {
   this.SortAlgorithm = sortAlgotirhm;
  }
  public string SortAlgorithm { get; set; }
 }
}
