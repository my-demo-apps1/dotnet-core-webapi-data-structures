﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class ArraySearchResult : ArraySearchParams
 {
  public ArraySearchResult() : base()
  {  }

  public ArraySearchResult(int[] numbersArray, int numberToSearch, int matchingIndex, List<string> searchLog) : base(numbersArray, numberToSearch)
  {
   this.MatchingIndex = matchingIndex;
   this.SearchLog = searchLog;
  }

  public int MatchingIndex { get; set; }
  public List<string> SearchLog { get; set; }

 }
}
