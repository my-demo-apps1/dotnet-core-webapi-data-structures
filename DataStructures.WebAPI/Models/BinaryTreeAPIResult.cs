﻿using DataStructures.Trees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataStructures.WebAPI.Models
{
 public class BinaryTreeAPIResult<T> where T : struct, IComparable
 {

  public BinaryTreeAPIResult(T[] inputArray, BinarySearchTree<T> bst)
  {
   Node<T> rootNode = bst.Root;
   this.BinaryTreeResult = new BinaryTreeResult<T>(inputArray, rootNode,
    TraverseNode<T>.GetInOrder(rootNode),
    TraverseNode<T>.GetPreOrder(rootNode),
    TraverseNode<T>.GetPostOrder(rootNode));
   Node<T> rootNodeBalanced = bst.GetBalancedTree();
   this.BalancedBinaryTreeResult = new BinaryTreeResult<T>(inputArray, rootNodeBalanced,
    TraverseNode<T>.GetInOrder(rootNodeBalanced),
    TraverseNode<T>.GetPreOrder(rootNodeBalanced),
    TraverseNode<T>.GetPostOrder(rootNodeBalanced)); ;
  }

  public BinaryTreeResult<T> BinaryTreeResult;
  public BinaryTreeResult<T> BalancedBinaryTreeResult;

 }
}
