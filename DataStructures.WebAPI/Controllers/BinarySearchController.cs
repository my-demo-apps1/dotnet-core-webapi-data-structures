﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStructures.WebAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataStructures.WebAPI.Controllers
{
 [Route("api/[controller]")]
 [EnableCors("ALLOW_ALL")]
 [ApiController]
 public class BinarySearchController : ControllerBase
 {

  [HttpPost]
  public ArraySearchResult Post([FromBody]ArraySearchParams value)
  {
   List<string> searchLog = new List<string>();
   int matchingIndex = SearchAlgorithms.BinarySearch(value.NumbersArray, value.NumberToSearch, searchLog);
   return new ArraySearchResult(value.NumbersArray, value.NumberToSearch, matchingIndex, searchLog);
  }
 }
}