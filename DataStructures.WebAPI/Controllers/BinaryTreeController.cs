﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStructures.Trees;
using DataStructures.WebAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataStructures.WebAPI.Controllers
{
 [Route("api/[controller]")]
 [EnableCors("ALLOW_ALL")]
 [ApiController]
 public class BinaryTreeController : ControllerBase
 {
  [HttpPost]
  public BinaryTreeAPIResult<int> Post([FromBody]ArrayParam<int> value)
  {
   BinarySearchTree<int> bst = new BinarySearchTree<int>();
   //int[] sortedNumbers = value.NumbersArray;
   //Array.Sort(sortedNumbers);
   foreach (int number in value.NumbersArray)
   {
    bst.Insert(number);
   }
   BinaryTreeAPIResult<int> result = new BinaryTreeAPIResult<int>(value.NumbersArray, bst);
   return result;
  }

 }
}