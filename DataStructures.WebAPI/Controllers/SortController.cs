﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStructures.Sorting;
using DataStructures.WebAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataStructures.WebAPI.Controllers
{
 [Route("api/[controller]")]
 [EnableCors("ALLOW_ALL")]
 [ApiController]
 public class SortController : ControllerBase
 {

  [HttpPost]
  public ArraySortResult Post([FromBody]Models.ArraySortParams value)
  {
   MergeSort<int> ms = new MergeSort<int>(value.NumbersArray);
   List<string> sortTrace = new List<string>();
   int[] resultArray = ms.Sort(sortTrace);

   return new ArraySortResult(value.NumbersArray, resultArray, value.SortAlgorithm, sortTrace);
  }
 }
 }