﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStructures.WebAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DataStructures.WebAPI.Controllers
{
 [Route("api/[controller]")]
 [EnableCors("ALLOW_ALL")]
 [ApiController]
 public class BalanceArrayController : ControllerBase
 {

  [HttpPost]
  public int[] Post([FromBody]ArrayParam<int> value)
  {
   return BalanceArray.Balance(value.NumbersArray);
  }

 }
}