﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using DataStructures.WebAPI.Models;
using System.Linq;

namespace DataStructures.WebAPI.Controllers.Tests
{
 [TestClass()]
 public class BinaryTreeControllerTests
 {
  [TestMethod()]
  public void PostTest_InOrder_Binary()
  {
   int[] numbersArray = { 8, 10, 12, 5, 3, 6 };
   int[] expectedResult = { 3, 5, 6, 8, 10, 12 };
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool inOrderEquals = expectedResult.SequenceEqual(actualResult.BinaryTreeResult.InOrder);
   Assert.IsTrue(inOrderEquals);
  }

  [TestMethod()]
  public void PostTest_PreOrder_Binary()
  {
   int[] numbersArray = { 8, 5, 3, 6, 10, 12 };
   int[] expectedResult = numbersArray;
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool preOrderEquals = expectedResult.SequenceEqual(actualResult.BinaryTreeResult.PreOrder);
   Assert.IsTrue(preOrderEquals);
  }

  [TestMethod()]
  public void PostTest_PostOrder_Binary()
  {
   int[] numbersArray = { 8, 10, 12, 5, 3, 6 };
   int[] expectedResult = { 3, 6, 5, 12, 10, 8 };
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool postOrderEquals = expectedResult.SequenceEqual(actualResult.BinaryTreeResult.PostOrder);
   Assert.IsTrue(postOrderEquals);
  }

  [TestMethod()]
  public void PostTest_InOrder_BalancedBinary()
  {
   int[] numbersArray = { 8, 10, 12, 5, 3, 6 };
   int[] expectedResult = { 3, 5, 6, 8, 10, 12 };
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool inOrderEquals = expectedResult.SequenceEqual(actualResult.BalancedBinaryTreeResult.InOrder);
   Assert.IsTrue(inOrderEquals);
  }

  [TestMethod()]
  public void PostTest_PreOrder_BalancedBinary()
  {
   int[] numbersArray = { 8, 5, 3, 6, 10, 12 };
   int[] expectedResult = { 8, 5, 3, 6, 12, 10 };
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool preOrderEquals = expectedResult.SequenceEqual(actualResult.BalancedBinaryTreeResult.PreOrder);
   Assert.IsTrue(preOrderEquals);
  }

  [TestMethod()]
  public void PostTest_PostOrder_BalancedBinary()
  {
   int[] numbersArray = { 8, 10, 12, 5, 3, 6 };
   int[] expectedResult = { 3, 6, 5, 10, 12, 8 };
   BinaryTreeAPIResult<int> actualResult = GetResult(numbersArray);
   bool postOrderEquals = expectedResult.SequenceEqual(actualResult.BalancedBinaryTreeResult.PostOrder);
   Assert.IsTrue(postOrderEquals);
  }

  protected BinaryTreeAPIResult<int> GetResult(int[] numbersArray)
  {
   ArrayParam<int> ap = new ArrayParam<int>(numbersArray);
   BinaryTreeController controller = new BinaryTreeController();
   return controller.Post(ap);
  }
 }
}