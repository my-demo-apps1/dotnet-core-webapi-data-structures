﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataStructures.WebAPI.Models;

namespace DataStructures.WebAPI.Controllers.Tests
{
 [TestClass()]
 public class BalanceArrayControllerTests
 {
  [TestMethod()]
  public void PostTest_BalanceByPrepend()
  {
   int[] numbersArray = { 1, 2, 1, 2, 1, 3, };
   int[] result = GetResult(numbersArray);
   Assert.AreEqual(result[0], 2);
  }

  [TestMethod()]
  public void PostTest_BalanceByAppend()
  {
   int[] numbersArray = { 2, 1, 3, 1, 2, 1 };
   int[] result = GetResult(numbersArray);
   Assert.AreEqual(result[result.Length - 1], 2);
  }

  [TestMethod()]
  public void PostTest_Balanced()
  {
   int[] numbersArray = { 2, 1, 3, 3, 1, 2 };
   int[] result = GetResult(numbersArray);
   bool equals = numbersArray.OrderBy(item => item).SequenceEqual(result.OrderBy(item => item));
   Assert.IsTrue(equals);
  }

  protected int[] GetResult(int[] numbersArray)
  {
   ArrayParam<int> ap = new ArrayParam<int>(numbersArray);
   BalanceArrayController controller = new BalanceArrayController();
   return controller.Post(ap);
  }
 }
}