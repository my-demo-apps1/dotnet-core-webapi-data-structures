﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures.WebAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using DataStructures.WebAPI.Models;

namespace DataStructures.WebAPI.Controllers.Tests
{
 [TestClass()]
 public class BinarySearchControllerTests
 {
  [TestMethod()]
  public void BinarySearchDisplayTest_NotFound()
  {
   int[] sortedArray = { 2, 3, 7, 11, 13, 19 };
   int numberToSearch = 20;
   int matchingIndex = GetResult(sortedArray, numberToSearch);
   Assert.AreEqual(matchingIndex, -1);
  }

  [TestMethod()]
  public void BinarySearchDisplayTest_Found()
  {
   int[] sortedArray = { 2, 3, 7, 11, 13, 19 };
   int numberToSearch = 11;
   int matchingIndex = GetResult(sortedArray, numberToSearch);
   Assert.AreEqual(matchingIndex, 3);
  }

  protected int GetResult(int[] sortedArray, int numberToSearch)
  {
   ArraySearchParams asp = new ArraySearchParams(sortedArray, numberToSearch);
   BinarySearchController controller = new BinarySearchController();
   ArraySearchResult result = controller.Post(asp);
   return result.MatchingIndex;
  }
 }
}