## dotnet-core-webapi-data-structures

This app is a demo of Datastructure Algorithms hosted as REST services using C# and .NET CORE WEBAPI
* C#
  * Generic Types
  * Unit Tests
* .NET Core Web API
  * CORS Configuration 
* Datastructure Algorithms
  * Binary Tree
  * Balanced Binary Tree
  * Binary Search on Array
  * Blancing Array
  * String Manipulataions
  * Sorting: Heap Sort, Quick Sort, Merge Sort

## Integration with Amazon Web Services
Hosted this API using AWS Lambda (Serverless hosting)
